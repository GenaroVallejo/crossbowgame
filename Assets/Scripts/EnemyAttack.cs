﻿using UnityEngine;
using System.Collections;

public class EnemyAttack : MonoBehaviour {

	public int enemyDamage = 1;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnCollisionEnter(Collision col)
	{
		Debug.Log ("collided with: " + col.gameObject);
		PlayerHealth playerHealthScript = col.gameObject.GetComponent<PlayerHealth> ();
		if (playerHealthScript != null) 
		{
			Debug.Log ("playerhealth: " + playerHealthScript);
			playerHealthScript.Damage(enemyDamage);
		}
	}

}
