﻿using UnityEngine;
using System.Collections;

public class EnemyHealth : MonoBehaviour, IDamageable {
	
	public int startingHealth = 3;
	
	private int currentHealth;
	
	void Start()
	{
		currentHealth = startingHealth;
	}
	
	public void Damage(int damage)
	{
		currentHealth -= damage;
		if (currentHealth <= 0) 
		{
			Defeated();
		}
	}
	
	void Defeated()
	{
		gameObject.SetActive (false);
	}
	
}