﻿using UnityEngine;
using System.Collections;

public class ProjectileShootScript : MonoBehaviour {
	
	
	public GameObject projectile;
	public Transform projectileSpawn;
	public float projectileForce = 600;
	public float fireRate;
	
	
	private float nextFire;
	
	
	// Update is called once per frame
	void Update () 
	{
		if (Input.GetButtonDown ("Fire1") && Time.time > nextFire) 
		{
			GameObject cloneProjectile = Instantiate(projectile, projectileSpawn.position, transform.rotation) as GameObject;
			Rigidbody cloneRb = cloneProjectile.GetComponent<Rigidbody>();
			cloneRb.AddForce(projectileSpawn.transform.forward * projectileForce);
		
			nextFire = Time.time + fireRate;
		}

		if (Input.GetKeyDown (KeyCode.P)) {
			
			if (	Cursor.lockState  ==  CursorLockMode.None) {
				
				Cursor.lockState = CursorLockMode.Locked;
				
			} else {
				
				Cursor.lockState = CursorLockMode.None;
				
			}
		}

	}
}
