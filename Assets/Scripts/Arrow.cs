﻿using UnityEngine;
using System.Collections;

public class Arrow : MonoBehaviour {

	private Collider myCollider;
	private Rigidbody rb;
	public float timeToDie = 5.0f;

	// Use this for initialization
	void Start () 
	{
		rb = GetComponent<Rigidbody> ();
		GameObject.Destroy(this.gameObject, timeToDie );
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void OnCollisionEnter (Collision other)
	{
		transform.SetParent (other.collider.transform);
		rb.isKinematic = true;
		Debug.Log ("other gameobject" + other.gameObject);
	}
}
