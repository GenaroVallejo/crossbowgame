﻿using UnityEngine;
using System.Collections;

public class PursueEnemy : MonoBehaviour {
	
	public Transform playerTransform;
	
	private NavMeshAgent agent;
	
	// Use this for initialization
	void Awake () {
		
		agent = GetComponent<NavMeshAgent> ();
	}
	
	// Update is called once per frame
	void Update () {
		
		agent.destination = playerTransform.position;
		
	}
}
