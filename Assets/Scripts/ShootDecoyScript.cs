﻿using UnityEngine;
using System.Collections;

public class ShootDecoyScript : MonoBehaviour {

	public GameObject projectile;
	public Transform projectileSpawn;
	public float projectileForce = 600;
	public float fireRate;
	
	
	private float nextFire;
	
	
	// Update is called once per frame
	void Update () 
	{
		if (Input.GetButtonDown ("Fire2") && Time.time > nextFire) 
		{
			GameObject cloneProjectile = Instantiate(projectile, projectileSpawn.position, transform.rotation) as GameObject;
			Rigidbody cloneRb = cloneProjectile.GetComponent<Rigidbody>();
			cloneRb.AddForce(projectileSpawn.transform.forward * projectileForce);
			
			nextFire = Time.time + fireRate;
		}
	}
}
