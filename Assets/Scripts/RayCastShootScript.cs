﻿using UnityEngine;
using System.Collections;

public class RayCastShootScript : MonoBehaviour {
	
	public float fireRate = .25f;
	public float weaponRange = 50;
	public ParticleSystem shootParticles;
	public GameObject hitParticles;
	public GameObject shootFlare;
	public int damage = 1;
	
	
	private Camera cam;
	private WaitForSeconds shotLength = new WaitForSeconds(.07f);
	private AudioSource source;
	private float nextFire;
	
	// Use this for initialization
	void Awake () 
	{
		source = GetComponent<AudioSource> ();
		cam = GetComponentInParent<Camera> ();
	}
	
	// Update is called once per frame
	void Update () 
	{
		RaycastHit hit;
		Vector3 rayOrigin = cam.ViewportToWorldPoint (new Vector3 (.5f, .5f, 0));
		if (Input.GetButtonDown ("Fire1") && Time.time > nextFire) 
		{
			nextFire = Time.time + fireRate;
			StartCoroutine(ShotEffect());
			if (Physics.Raycast (rayOrigin, cam.transform.forward, out hit, weaponRange))
			{
				IDamageable dmgScript = hit.collider.GetComponent<IDamageable>();
				if (dmgScript != null)
				{
					Instantiate (hitParticles, hit.point, Quaternion.identity);
					dmgScript.Damage(damage);
				}
			}
		}
	}
	
	
	private IEnumerator ShotEffect()
	{
		source.Play ();
		shootParticles.Play ();
		shootFlare.SetActive (true);
		
		yield return shotLength;
		
		shootFlare.SetActive (false);
	}
}