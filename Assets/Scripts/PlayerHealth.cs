﻿using UnityEngine;
using System.Collections;

public class PlayerHealth : MonoBehaviour,IDamageable {
			
	public int startingHealth = 3;
	
	private int currentHealth;
	private bool tookDamage;
	
	void Start()
	{
		currentHealth = startingHealth;
	}

	void OnCollisionEnter(Collision col)
	{
		Debug.Log ("player collided with: " + col.gameObject);
	}

	public void Damage(int damage)
	{
		tookDamage = true;

		if (tookDamage == true) {
			tookDamage = false;
			Invoke ( "currentHealth", 2);

		}


		currentHealth -= damage;
		if (currentHealth <= 0) 
		{
			Defeated();
		}
	}
	
	void Defeated()
	{
		gameObject.SetActive (false);
	}
	
}